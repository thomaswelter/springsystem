import { Spring } from './Spring.js'

export class Axis {
	constructor() {
		this.value = 0
		this.velocity = 0
		this.mass = 1
		this.damping = 0.7
		this.springs = new Set
		this.limit = 0.01
	}

	addSpring(position) {
		const spring = new Spring(position)
		this.springs.add(spring)
		return spring
	}

	removeSpring(spring) {
		this.springs.remove(spring)
	}

	step() {
		let force = 0
		for(const spring of this.springs)
			force += spring.getForce(this.value)

		this.velocity += force * this.mass
		this.velocity *= this.damping

		this.value += this.velocity
	}
}