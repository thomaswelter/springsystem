import { Axis } from './Axis.js'

export class SpringSystem {
	constructor(element) {
		this.element = element

		this.x = new Axis
		this.y = new Axis
		this.scale = new Axis
		this.scale.value = 1

		this.rotation = new Axis

		this.opacity = new Axis
		this.opacity.value = 1
		this.opacity.damping = 0.4

		this.render()
	}

	render() {
		requestAnimationFrame(this.render.bind(this))

		this.x.step()
		this.y.step()
		this.scale.step()
		this.rotation.step()
		this.opacity.step()

		this.element.style.transform = `
			translate(${this.x.value}px, ${this.y.value}px)
			rotate(${this.rotation.value}deg)
			scale(${this.scale.value})
		`

		this.element.style.opacity = this.opacity.value
	}
}