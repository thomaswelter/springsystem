export class Spring {
	constructor(position = 0) {
		this.position = position
		this.stiffness = 0.3
		this.active = true
		this.maxForce = 0
		this.maxDistance = 0
	}

	getForce(value) {
		if(!this.active) return 0

		const displacement = value - this.position
		const force = displacement * -this.stiffness

		if(this.maxDistance > 0 && Math.abs(displacement) > this.maxDistance)
			this.active = false

		if(this.maxForce > 0 && Math.abs(force) > this.maxForce)
			this.active = false
		
		if(!this.active) return 0

		return force
	}
}