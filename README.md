# Spring System
Make html elements springy

[Demo](https://thomaswelter.gitlab.io/springsystem)

# The basics
This example makes the element follow the pointer in a springy way
```javascript
import { SpringSystem } from './SpringSystem.js'

// create system from element
const system = new SpringSystem(element)

// add springs for x and y
const x = system.x.addSpring()
const y = system.y.addSpring()

// update the spring positions
window.addEventlistener('pointermove', ev => {
    x.position = ev.pageX
    y.position = ev.pageY
})
```

# API
- `new SpringSystem( element )`

  create new system from element. 
  
  - `SpringSystem.x`            Axis
  - `SpringSystem.y`            Axis
  - `SpringSystem.scale`        Axis
  - `SpringSystem.rotation`     Axis
  - `SpringSystem.opacity`      Axis

- `new Axis`

  think of it as a line, the value is a point on this line. You can attach multiple springs to different points and the value will be pulled towards it
  
  - `Axis.value`                The current value
  - `Axis.velocity`             The current velocity
  - `Axis.mass`                 Change the overall stiffness
  - `Axis.damping`              How fast the wobble fades out
  - `Axis.addSpring(position)`  Returns `Spring` add position
  - `Axis.removeSpring(Spring)` Remove spring
  
- `new Spring`

  An instance of this is returned from `Axis.addSpring`
  
  - `Spring.position`           The anchor point of the spring
  - `Spring.stiffness`          How hard the spring pulls
  - `Spring.active`             Wether this spring should pull
  - `Spring.maxForce`           When not zero and if force is higher then this the spring will be deactivated.
  - `Spring.maxDistance`        When not zero and if force is higher then this the spring will be deactivated.
  