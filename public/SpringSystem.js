class Spring {
	constructor(position = 0) {
		this.position = position;
		this.stiffness = 0.3;
		this.active = true;
		this.maxForce = 0;
		this.maxDistance = 0;
	}

	getForce(value) {
		if(!this.active) return 0

		const displacement = value - this.position;
		const force = displacement * -this.stiffness;

		if(this.maxDistance > 0 && Math.abs(displacement) > this.maxDistance)
			this.active = false;

		if(this.maxForce > 0 && Math.abs(force) > this.maxForce)
			this.active = false;
		
		if(!this.active) return 0

		return force
	}
}

class Axis {
	constructor() {
		this.value = 0;
		this.velocity = 0;
		this.mass = 1;
		this.damping = 0.7;
		this.springs = new Set;
		this.limit = 0.01;
	}

	addSpring(position) {
		const spring = new Spring(position);
		this.springs.add(spring);
		return spring
	}

	removeSpring(spring) {
		this.springs.remove(spring);
	}

	step() {
		let force = 0;
		for(const spring of this.springs)
			force += spring.getForce(this.value);

		this.velocity += force * this.mass;
		this.velocity *= this.damping;

		this.value += this.velocity;
	}
}

class SpringSystem {
	constructor(element) {
		this.element = element;

		this.x = new Axis;
		this.y = new Axis;
		this.scale = new Axis;
		this.scale.value = 1;

		this.rotation = new Axis;

		this.opacity = new Axis;
		this.opacity.value = 1;
		this.opacity.damping = 0.4;

		this.render();
	}

	render() {
		requestAnimationFrame(this.render.bind(this));

		this.x.step();
		this.y.step();
		this.scale.step();
		this.rotation.step();
		this.opacity.step();

		this.element.style.transform = `
			translate(${this.x.value}px, ${this.y.value}px)
			rotate(${this.rotation.value}deg)
			scale(${this.scale.value})
		`;

		this.element.style.opacity = this.opacity.value;
	}
}

export { SpringSystem };
